#!/bin/bash
# Purpose: Block all traffic from AFGHANISTAN (af) and CHINA (CN). Use ISO code. #
# See url for more info - http://www.cyberciti.biz/faq/?p=3402
# Author: nixCraft <www.cyberciti.biz> under GPL v.2.0+
# -------------------------------------------------------------------------------
ISO="af cn ua"

### Set PATH ###
IPT=/sbin/iptables
CURL=/usr/bin/curl
EGREP=/usr/bin/egrep

### No editing below ###
SPAMLIST="countrydrop"
ZONEROOT="/root/iptables"
DLROOT="http://www.ipdeny.com/ipblocks/data/countries"

cleanOldRules(){
	$IPT -F
	$IPT -X
	$IPT -t nat -F
	$IPT -t nat -X
	$IPT -t mangle -F
	$IPT -t mangle -X
	$IPT -P INPUT ACCEPT
	$IPT -P OUTPUT ACCEPT
	$IPT -P FORWARD ACCEPT
}

# create a dir
[ ! -d $ZONEROOT ] && /bin/mkdir -p $ZONEROOT

# clean old rules
cleanOldRules

# create a new iptables list
echo "Criando lista do IPTables..."
$IPT -N $SPAMLIST

for c  in $ISO
do
	#echo "Bloqueando $c..."

	# local zone file
	tDB=$ZONEROOT/$c.zone

	# get fresh zone file
	$CURK -s -o $tDB $DLROOT/$c.zone

	# country specific log message
	SPAMDROPMSG="País Bloqueado: [$c]"

	# get
	BADIPS=$(egrep -v "^#|^$" $tDB)
	for ipblock in $BADIPS
	do
	   $IPT -A $SPAMLIST -s $ipblock -j LOG --log-prefix "$SPAMDROPMSG"
	   $IPT -A $SPAMLIST -s $ipblock -j DROP
	done

done

oDB=$ZONEROOT/custom.zone

if [ -f $oDB ]; then
	BADIPS=$(egrep -v "^#|^$" $oDB)
	for ipblock in $BADIPS
	do
		echo "Bloqueando o IP: $ipblock..."
    	$IPT -A $SPAMLIST -s $ipblock -j LOG --log-prefix "$SPAMDROPMSG"
    	$IPT -A $SPAMLIST -s $ipblock -j DROP
	done
fi

# Drop everything
$IPT -I INPUT -j $SPAMLIST
$IPT -I OUTPUT -j $SPAMLIST
$IPT -I FORWARD -j $SPAMLIST

# call your other iptable script
# /path/to/other/iptables.sh

exit 0
