#!/usr/bin/env bash
#
# syncmon.sh - Um programa para sincronizar automáticamente arquivos alterados em alguma pasta
# usando o rsync e inotifywait.
#
# Criador por Lucas Saliés Brum a.k.a. sistematico, <lucas at archlinux dot com dot br>
#
# Criado em: 15/03/2018 18:15:04
# Última alteração: 15/05/2019 07:42:17

command -v inotifywait >/dev/null 2>&1 || { echo >&2 "O aplicativo inotifywait não está instalado. Abortando."; exit 1; }

if [ ! $1 ]; then
	echo "Uso: $(basename $0) /pasta/ 'comando'"
else
	while true; do
		inotifywait -q -e modify,create,delete -r $1 && $2 && echo "[$(date +'%Y-%m-%d %H:%M')] - Arquivo alterado..." >> /var/log/syncmon.log
	done
fi

